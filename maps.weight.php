<?php include 'header.php'; ?>
<?php include 'header.php'; ?>

<script src="http://maps.google.com/maps/api/js?sensor=false" 
          type="text/javascript"></script>
</head> 
<body>
<div class="col-md-6 col-md-offset-2">
<h2>Best Weighting Excercise Sites</h2>
  <div id="map" style="width: 800px; height: 800px; border:solid #ccc 3px;"></div>
</div>

  <script type="text/javascript">
    var locations = [
      ['Fitness Buff Gym', 14.5750311,121.0414981,18],
      ['Golds Gym Athletics', 14.5756999,121.0536853,18],
      ['Anytime Fitness', 14.5513198,121.0481948,15.75],
      ['Golds Gym',14.552022,121.0498704,17],
      ['360 Fitness Plus Fort', 14.5643591,121.0417173,14]
    ];

    var map = new google.maps.Map(document.getElementById('map'), {
      zoom: 10,
      center: new google.maps.LatLng(14.5135378,121.0654038,13),
      mapTypeId: google.maps.MapTypeId.ROADMAP
    });

    var infowindow = new google.maps.InfoWindow();

    var marker, i;

    for (i = 0; i < locations.length; i++) {  
      marker = new google.maps.Marker({
        position: new google.maps.LatLng(locations[i][1], locations[i][2]),
        map: map
      });

      google.maps.event.addListener(marker, 'click', (function(marker, i) {
        return function() {
          infowindow.setContent(locations[i][0]);
          infowindow.open(map, marker);
        }
      })(marker, i));
    }
  </script>

      <div class="col-md-12 nopad"> 
        <?php include 'footer.php';?>
      </div>