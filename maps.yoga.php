<?php include 'header.php'; ?>

<script src="http://maps.google.com/maps/api/js?sensor=false" type="text/javascript"></script>

</head> 
<body>
<div class="col-md-6 col-md-offset-2">

  <div id="map" style="width: 800px; height: 800px; border:solid #ccc 3px;"></div>
</div>
  
  <script type="text/javascript">
    var locations = [
      ['Yoga Philippines',14.5639959,121.0563139,17],
      ['Ananda Marga Yoga Center ', 14.4453156,120.9561033,12],
      ['Yoga Plus', 14.5541718,121.0445857,17],
      ['Beyond Yoga Philippines',14.5541666,120.9878798,12],
      ['Bliss Yoga', 14.5541666,120.9878798,12]
    ];

    var map = new google.maps.Map(document.getElementById('map'), {
      zoom: 10,
      center: new google.maps.LatLng(14.5135378,121.0654038,13),
      mapTypeId: google.maps.MapTypeId.ROADMAP
    });

    var infowindow = new google.maps.InfoWindow();

    var marker, i;

    for (i = 0; i < locations.length; i++) {  
      marker = new google.maps.Marker({
        position: new google.maps.LatLng(locations[i][1], locations[i][2]),
        map: map
      });

      google.maps.event.addListener(marker, 'click', (function(marker, i) {
        return function() {
          infowindow.setContent(locations[i][0]);
          infowindow.open(map, marker);
        }
      })(marker, i));
    }
  </script>


 <div class="col-md-12 nopad"> 
<?php include 'footer.php';?>
</div>