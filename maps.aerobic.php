<?php include 'header.php'; ?>
<?php include 'header.php'; ?>

<script src="http://maps.google.com/maps/api/js?sensor=false" 
          type="text/javascript"></script>
</head> 
<body>
<div class="col-md-6 col-md-offset-2">
<h2>Best Aerobics Excercise Sites</h2>
  <div id="map" style="width: 800px; height: 800px; border:solid #ccc 3px;"></div>
</div>

  <script type="text/javascript">
    var locations = [
      ['Ftx Fitness Exchange', 14.5316443,121.0359098,13],
      ['Yoga +', 14.5851851,121.0597505,16.5],
      ['Brgy. Ususan Covered Court', 14.5355208,121.0668443,17],
      ['Vista Mall Taguig',14.5307085,121.0719347,17],
      ['Makati Park and Garden', 14.5643079,121.0548496,16]
    ];

    var map = new google.maps.Map(document.getElementById('map'), {
      zoom: 10,
      center: new google.maps.LatLng(14.5135378,121.0654038,13),
      mapTypeId: google.maps.MapTypeId.ROADMAP
    });

    var infowindow = new google.maps.InfoWindow();

    var marker, i;

    for (i = 0; i < locations.length; i++) {  
      marker = new google.maps.Marker({
        position: new google.maps.LatLng(locations[i][1], locations[i][2]),
        map: map
      });

      google.maps.event.addListener(marker, 'click', (function(marker, i) {
        return function() {
          infowindow.setContent(locations[i][0]);
          infowindow.open(map, marker);
        }
      })(marker, i));
    }
  </script>

      <div class="col-md-12 nopad"> 
        <?php include 'footer.php';?>
      </div>