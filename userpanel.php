<?php include 'header.php'; 

 if(!(isset($_SESSION["patientID"]))){
 	header("location:index.php");
 }


 ?>
<script>
	$(document).ready(function(){
	
			$(".jogging-laman").hide();	
				$(".jogging-button").click(function(){
					$(".jogging-laman").toggle("slow");
				});		


			$(".cycling-laman").hide();	
				$(".cycling-button").click(function(){
					$(".cycling-laman").toggle("slow");
				});	


			$(".swimming-laman").hide();	
				$(".swimming-button").click(function(){
					$(".swimming-laman").toggle("slow");
				});		

			$(".motion-laman").hide();	
				$(".motion-button").click(function(){
					$(".motion-laman").toggle("slow");
				});	

			$(".aerobic-laman").hide();	
				$(".aerobic-button").click(function(){
					$(".aerobic-laman").toggle("slow");
				});

			$(".walking-laman").hide();	
				$(".walking-button").click(function(){
					$(".walking-laman").toggle("slow");
				});

			$(".taichi-laman").hide();	
				$(".taichi-button").click(function(){
					$(".taichi-laman").toggle("slow");
				});				

			$(".weight-laman").hide();	
				$(".weight-button").click(function(){
					$(".weight-laman").toggle("slow");
				});		

			$(".yoga-laman").hide();	
				$(".yoga-button").click(function(){
					$(".yoga-laman").toggle("slow");
				});		

			$(".warmup-laman").hide();	
				$(".warmup-button").click(function(){
					$(".warmup-laman").toggle("slow");
				});		


	});
</script>


<div class="container">


	<div class="col-md-12 mtop-30">
		
		<div class="col-md-4">
			<div class="col-md-12 green">
			
				Exercise
		
			</div>

			<div class="col-md-12 nopad panel pad-10">
				<?php  if($row['sicknessID']==1 || $row['sicknessID']==2 || $row['sicknessID']==4){ ?>

				<div class="col-md-10"><b>Jogging</b></div>
				<div class="col-md-2"><button class="jogging-button btn btn-default"><i class="fa fa-caret-down"></i></button></div>
				<div class="col-md-12 jogging-laman ">
						<p >When you jog, the bad cholesterol in your body is reduced and as a result
							 you happen to reduce weight. Your body mass index [BMI] also decreases.
							 Once your weight is reduced, your blood pressure and glucose level also tend to reduce.
							 It helps fight mental stress or depression.</p>
				
				<center><a  class="btn btn-info" href="maps.jogging.php">Show Maps</a></center>
				</div>

				<div class="col-md-12 border-bot"></div>

				<?php }?>
				<?php  if($row['sicknessID']==1 || $row['sicknessID']==2 || $row['sicknessID']==3){ ?>
				<div class="col-md-10"><b>Cycling</b></div>
				<div class="col-md-2"><button class="cycling-button btn btn-default"><i class="fa fa-caret-down"></i></button></div>
				<div class="col-md-12 cycling-laman">
						<p >Cycling improves your immune system and keeps you at a safe distance
							from infectious diseases. It regulates blood circulation and is good
							for your cardiovascular muscles thereby preventing you from cardiovascular diseases.</p>


				<center><a  class="btn btn-info" href="maps.cycling.php">Show Maps</a></center>
				</div>

				<div class="col-md-12 border-bot"></div>

				<?php }?>
				<?php  if($row['sicknessID']==1 || $row['sicknessID']==5){ ?>
				<div class="col-md-10"><b>Swimming</b></div>
				<div class="col-md-2"><button class="swimming-button btn btn-default"><i class="fa fa-caret-down"></i></button></div>
				<div class="col-md-12 swimming-laman">
						<p >Cycling improves your immune system and keeps you at a safe distance
							from infectious diseases. It regulates blood circulation and is good
							for your cardiovascular muscles thereby preventing you from cardiovascular diseases.</p>

				<center><a  class="btn btn-info" href="maps.swimming.php">Show Maps</a></center>

				</div>

				<div class="col-md-12 border-bot"></div>

				<?php }?>
				<?php  if($row['sicknessID']==1 || $row['sicknessID']==2 || $row['sicknessID']==5){ ?>
					
				<div class="col-md-10"><b>Range of Motion</b></div>
				<div class="col-md-2"><button class="motion-button btn btn-default"><i class="fa fa-caret-down"></i></button></div>
				<div class="col-md-12 motion-laman">
						<p >refers to the ability to move your joints through the full motion they were designed to
						 achieve. Range-of-motion exercises include gentle stretching and movements that take joints
						 through their full span. Doing these exercises regularly – ideally every day – can help maintain
						 and even improve the flexibility in your joints.</p>
				
				<center><a  class="btn btn-info" href="maps.jogging.php">Show Maps</a></center>
				</div>


				<div class="col-md-12 border-bot"></div>

				<?php }?>
				<?php  if($row['sicknessID']==1 || $row['sicknessID']==2 || $row['sicknessID']==5){ ?>

				<div class="col-md-10"><b>Aerobic Exercises</b></div>
				<div class="col-md-2"><button class="aerobic-button btn btn-default"><i class="fa fa-caret-down"></i></button></div>
				<div class="col-md-12 aerobic-laman">
						<p >Aerobic exercises strengthen your heart and make your lungs more efficient. This conditioning has the added benefit of reducing fatigue, so you have more stamina throughout the day. Aerobic exercise also helps control your weight by increasing the amount of calories your body uses.
							150 minutes of moderate-intensity aerobic exercise per week
							OR
							75 minutes of vigorous-intensity aerobic exercise per week
							OR
							an equivalent combination of moderate and vigorous exercise.</p>
				
				<center><a  class="btn btn-info" href="maps.aerobic.php">Show Maps</a></center>
				</div>

				<div class="col-md-12 border-bot"></div>

				<?php }?>
				<?php  if($row['sicknessID']==1 || $row['sicknessID']==2 || $row['sicknessID']==5 || $row['sicknessID']==4){ ?>

				<div class="col-md-10"><b>Walking Exercises</b></div>
				<div class="col-md-2"><button class="walking-button btn btn-default"><i class="fa fa-caret-down"></i></button></div>
				<div class="col-md-12 walking-laman">
						<p >Walking is probably one of the most prescribed activities for people with type 2 diabetes.
						Brisk walking done at a pace to raise the heart rate is an aerobic exercise, and studies show
						beneficial effects when people with diabetes participate in aerobic activities at least three
						days a week for a total of 150 minutes. The American Diabetes Association (ADA) recommends people
						not go more than two consecutive days without an aerobic exercise session.</p>
				
				<center><a  class="btn btn-info" href="maps.jogging.php">Show Maps</a></center>
				</div>

				<div class="col-md-12 border-bot"></div>

				<?php }?>
				<?php  if($row['sicknessID']==5){ ?>
				<div class="col-md-10"><b>Tai chi</b></div>
				<div class="col-md-2"><button class="taichi-button btn btn-default"><i class="fa fa-caret-down"></i></button></div>
				<div class="col-md-12 taichi-laman">
						<p >Tai chi, a series of movements performed in a slow and relaxed manner over 30 minutes,
						 has been practiced for centuries. At least one small study has confirmed it is an excellent
						 choice of exercise for type 2 diabetes. Tai chi is ideal for people with diabetes because
						 it provides fitness and stress reduction in one. Tai chi also improves balance and may reduce
						 nerve damage, a common diabetic complication, although the latter benefit remains unproven.</p>
				
				<center><a  class="btn btn-info" href="maps.taichi.php">Show Maps</a></center>
				</div>

				<div class="col-md-12 border-bot"></div>


				<?php }?>
				<?php  if($row['sicknessID']==1 || $row['sicknessID']==2 || $row['sicknessID']==5){ ?>

				<div class="col-md-10"><b>Weight Training</b></div>
				<div class="col-md-2"><button class="weight-button btn btn-default"><i class="fa fa-caret-down"></i></button></div>
				<div class="col-md-12 weight-laman">
						<p >Weight training builds muscle mass, important for those with type 2 diabetes.
						Plan for resistance exercise or weight training at least twice a week as part of
						your diabetic management plan — three is ideal, but always schedule a rest day 
						between weight workouts (other exercise is fine on those days). Each session should
						include 5 to 10 different types of lifting involving the major muscle groups. For
						optimal strength gains, work your way up to doing three to four sets of each exercise,
						with each set comprising 10 to 15 repetitions.</p>
				
				<center><a  class="btn btn-info" href="maps.weight.php">Show Maps</a></center>
				</div>

				<div class="col-md-12 border-bot"></div>

				<?php }?>
				<?php  if($row['sicknessID']==1 || $row['sicknessID']==2 || $row['sicknessID']==3){ ?>

				<div class="col-md-10"><b>Yoga</b></div>
				<div class="col-md-2"><button class="yoga-button btn btn-default"><i class="fa fa-caret-down"></i></button></div>
				<div class="col-md-12 yoga-laman">
						<p >Yoga can help lower body fat, fight insulin resistance, and improve nerve function
						 — all important when you have type 2 diabetes. Like tai chi, yoga is also a great diabetic stress reducer.
						 One of the advantages of yoga as an exercise is that you can do it as often as you like.</p>
				
				<center><a  class="btn btn-info" href="maps.yoga.php">Show Maps</a></center>
				</div>

				<div class="col-md-12 border-bot"></div>

				<?php }?>
				<?php  if($row['sicknessID']==1 || $row['sicknessID']==2 || $row['sicknessID']==5){ ?>
				<div class="col-md-10"><b>Perform Warm-up Exercises</b></div>
				<div class="col-md-2"><button class="warmup-button btn btn-default"><i class="fa fa-caret-down"></i></button></div>
				<div class="col-md-12 warmup-laman">
						<p >-Perform warm-up exercises, and maintain an appropriate cool-down period after exercise.
						-If the weather is cold, exercise indoors or wear a mask or scarf over your nose and mouth.
						-Exercise at a level that is appropriate for you.</p>
				
				<center><a  class="btn btn-info" href="maps.jogging.php">Show Maps</a></center>
				</div>

				<div class="col-md-12 border-bot"></div>
					<?php }?>	
				
				
			</div>


		</div>


		<div class="col-md-4">
			<div class="col-md-12 red">
			
				Medication
		
			</div>

			<div class="col-md-12 nopad panel pad-10">
			 <?php 
			 if($row['sicknessID']==1 || $row['sicknessID']==3 || $row['sicknessID']==5){

			 ?>
				<div class="col-md-12">
					<div class="col-md-12"><b>Paracetamol</b></div>
					<div class="col-md-6"><p>Dosage</p></div>
					<div class="col-md-6"><p>Every 4 Hours</p></div>
				</div>
				


			
				<div class="col-md-12 border-bot"></div>

				<div class="col-md-12">
					<div class="col-md-12"><b>Amoxicillin</b></div>
					<div class="col-md-6"><p>Dosage</p></div>
					<div class="col-md-6"><p>3 Times a day</p></div>
				</div>
				

				<div class="col-md-12 border-bot"></div>

				<div class="col-md-12">
					<div class="col-md-12"><b>Hydro Codone</b></div>
					<div class="col-md-6"><p>Dosage</p></div>
					<div class="col-md-6"><p>2 Times a day</p></div>
				</div>				

				<?php }else{?>

				<div class="col-md-12 border-bot"></div>

				<div class="col-md-12">
					<div class="col-md-12"><b>Mefenamic Acid</b></div>
					<div class="col-md-6"><p>Dosage</p></div>
					<div class="col-md-6"><p>3 Times a day</p></div>
				</div>				

				<div class="col-md-12 border-bot"></div>

				<div class="col-md-12">
					<div class="col-md-12"><b>Acetaminophen</b></div>
					<div class="col-md-6"><p>Dosage</p></div>
					<div class="col-md-6"><p>2 pills every 4 to 6 hours</p></div>
				</div>		

				<div class="col-md-12 border-bot"></div>

				<div class="col-md-12">
					<div class="col-md-12"><b>Ibuprofen</b></div>
					<div class="col-md-6"><p>Dosage</p></div>
					<div class="col-md-6"><p>1 pills every 4 to 6 hours</p></div>
				</div>	

			<?php }?>

			</div>

		</div>




		<div class="col-md-4">
			<div class="col-md-12 violet">
			
				Foods
		
			</div>

			<div class="col-md-12 nopad panel pad-10">
				<div class="col-md-10"><b>Foods to Take</b>
					
				</div>
				<div class="col-md-12 ">
				<?php 
					$get_food_take = mysqli_query($conn, "SELECT * from tbl_food_take where sicknessID = '".$row['sicknessID']."'");
					while($rows = mysqli_fetch_assoc($get_food_take))
					{
						echo "<p>".$rows['foodname']."</p>";
					}

				?>
				
				</div>

				
				<div class="col-md-12 border-bot"></div>

				<div class="col-md-10"><b>Foods to Avoid</b>
					
				</div>
				<div class="col-md-12 ">
						<?php 
							$get_food_take = mysqli_query($conn, "SELECT * from tbl_food_avoid where sicknessID = '".$row['sicknessID']."'");
							while($rows = mysqli_fetch_assoc($get_food_take))
							{
								echo "<p>".$rows['foodname']."</p>";
							}

						?>
				
				</div>

				


			</div>

			

				

			</div>

		</div>



	</div>

</div>


<?php include 'footer.php';?>