
<script src="http://maps.google.com/maps/api/js?sensor=false" 
          type="text/javascript"></script>
</head> 
<body>
  <div id="map" style="width: 800px; height: 800px;"></div>

  <script type="text/javascript">
    var locations = [
      ['Capitol Commons', 14.5756037,121.0607195, 17],
      ['Mairkina Sports Center', 14.6353842,121.0966998,17],
      ['Marikina River Park', 14.6350441,121.091614,17],
      ['Greenhills Shopping Compund',14.6353654,121.049916,12.75],
      ['Global Circuit Makati', 14.5661502,121.0099054,15]
    ];

    var map = new google.maps.Map(document.getElementById('map'), {
      zoom: 10,
      center: new google.maps.LatLng(14.5135378,121.0654038,13),
      mapTypeId: google.maps.MapTypeId.ROADMAP
    });

    var infowindow = new google.maps.InfoWindow();

    var marker, i;

    for (i = 0; i < locations.length; i++) {  
      marker = new google.maps.Marker({
        position: new google.maps.LatLng(locations[i][1], locations[i][2]),
        map: map
      });

      google.maps.event.addListener(marker, 'click', (function(marker, i) {
        return function() {
          infowindow.setContent(locations[i][0]);
          infowindow.open(map, marker);
        }
      })(marker, i));
    }
  </script>