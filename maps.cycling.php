<?php include 'header.php'; ?>

<script src="http://maps.google.com/maps/api/js?sensor=false" 
          type="text/javascript"></script>
</head> 
<body>
<div class="col-md-6 col-md-offset-2">
<h2>Best Cycling Sites</h2>
  <div id="map" style="width: 800px; height: 800px;"></div>
</div>

  <script type="text/javascript">
    var locations = [
      ['Mapacagal Boulevard',14.5405229,120.9860533,17],
      ['Coregidor Island', 14.3832471,120.5746206,14],
      ['Timberland Avenue', 14.6302391,121.058168,13],
      ['La Mesa Dam',14.7124423,121.0752047,18],
      ['University of the Phillipines', 14.6550077,121.0627635,17]
    ];

    var map = new google.maps.Map(document.getElementById('map'), {
      zoom: 10,
      center: new google.maps.LatLng(14.5135378,121.0654038,13),
      mapTypeId: google.maps.MapTypeId.ROADMAP
    });

    var infowindow = new google.maps.InfoWindow();

    var marker, i;

    for (i = 0; i < locations.length; i++) {  
      marker = new google.maps.Marker({
        position: new google.maps.LatLng(locations[i][1], locations[i][2]),
        map: map
      });

      google.maps.event.addListener(marker, 'click', (function(marker, i) {
        return function() {
          infowindow.setContent(locations[i][0]);
          infowindow.open(map, marker);
        }
      })(marker, i));
    }
  </script>