<?php
include'config.php';
?>
<!DOCTYPE html>
<html>
<head>

	<title>My Health Companion</title>
	<link rel="stylesheet" type="text/css" href="assets/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="assets/css/style.css">
	<link rel="stylesheet" type="text/css" href="assets/fonts/stylesheet.css">
	<link rel="stylesheet" type="text/css" href="assets/css/icons/fontawesome/styles.min.css">
	<link rel="stylesheet" type="text/css" href="assets/css/fonts/style.css">
	<script type="text/javascript" src="assets/js/jquery-2.1.4.min.js"></script>
	<script type="text/javascript" src="assets/js/bootstrap.min.js"></script>


</head>
<body>




<nav class="navbar navbar" id="nav">
  <div class="container-fluid" >
    <div class="navbar-header">

      <a class="navbar-brand" href="../uhac"><img src="assets/images/logago.png" class="logo"></a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav">
      <?php if(isset($_SESSION["patientID"])){?>

      	<?php 
      		$get_data = mysqli_query($conn, "SELECT * from tbl_medical_records where patientID = '".$_SESSION["patientID"]."'");
      		$row = mysqli_fetch_assoc($get_data);


      	?>



        <li class="active"><a href="userpanel.php">Home</a></li>
        <li><a href="/uhac/medical-records.php">Medical Records</a></li>
        
        
        <?php }else{?>
        	<li><a href="#">About Us</a></li> 
        	<li><a href="#">Contact Us</a></li>
        <?php }?>
      </ul>

       <?php if(isset($_SESSION["patientID"])){?>
      <ul class="nav navbar-nav navbar-right">
        <li><a href="logout.php"> <?php echo $row['name']?> <i class="fa fa-power-off"></i></a> </li>
       
      </ul>
      <?php }?>
    </div>
  </div>
</nav>

<script>
$(document).ready(function(){
		$("#notif").hide();	
	});	



setInterval(function () {

$(document).ready(function(){
		$("#notif").show();	
		$("#notif").fadeOut(10000);
		$(".buz").trigger('play');
		
		
	});	

	 }, 20000);


						

</script>




 <?php if(isset($_SESSION["patientID"])){?>
<div class="notify bg-danger" id="notif">
	I'TS TIME TO TAKE YOUR MEDICATION

</div>

<audio class="buz" controls preload="none" style="display:none"> 
   <source src="assets/speech.mp3" type="audio/mpeg">
</audio>

<?php }?>