<?php include 'header.php'; ?>

<script src="http://maps.google.com/maps/api/js?sensor=false" 
          type="text/javascript"></script>
</head> 
<body>
<div class="col-md-6 col-md-offset-2">
<h2>Best Cycling Sites</h2>
  <div id="map" style="width: 800px; height: 800px; border:solid #ccc 3px;"></div>
</div>

  <script type="text/javascript">
    var locations = [
      ['Makati Aqua Sport Arena',14.5639959,121.0563139,17],
      ['Philippine Army Wellness Center', 14.5339512,121.0405273,17],
      ['PhilSports Complex', 14.5790121,121.065738,18],
      ['Ace Water Spa',14.5733402,121.0568397,17],
      ['Rizal Memorial Sports Complex', 14.562816,120.9919027,18]
    ];

    var map = new google.maps.Map(document.getElementById('map'), {
      zoom: 10,
      center: new google.maps.LatLng(14.5135378,121.0654038,13),
      mapTypeId: google.maps.MapTypeId.ROADMAP
    });

    var infowindow = new google.maps.InfoWindow();

    var marker, i;

    for (i = 0; i < locations.length; i++) {  
      marker = new google.maps.Marker({
        position: new google.maps.LatLng(locations[i][1], locations[i][2]),
        map: map
      });

      google.maps.event.addListener(marker, 'click', (function(marker, i) {
        return function() {
          infowindow.setContent(locations[i][0]);
          infowindow.open(map, marker);
        }
      })(marker, i));
    }
  </script>