<!DOCTYPE html>
<html>
<head>

	<title>My Health Companion</title>
	<link rel="stylesheet" type="text/css" href="assets/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="assets/css/style.css">
	<style type="text/css">
		a{
			color: blue;
		}
		a:hover{
			color: darkred;
		}
		.mtop
		{
			margin-top: 100px;
		}
	</style>
</head>
<body>
	<div class="container-fluid footer-bg2 mtop">
		<row>
			<div class="col-md-3">
			</div>

				
				<div class="col-md-4">
					<row>
						<div class="col-md-12">
							<font size="5px" color="white">Contact Us</font>
						</div>
					</row>
					<row>
						<div class="col-md-12">
							<font size="4px" color="white">&nbsp;&nbsp;<img src= "assets/images/phone.png" width="20px" height="20px">&nbsp;Call</font><font color="blue" size="4px"> +639153388995</font>
						</div>
					</row>
					<row>
						<div class="col-md-12">
							<font size="4px" color="white">&nbsp;&nbsp;<img src= "assets/images/icon-mail.png" width="20px" height="20px">&nbsp;Email <a href="#">codebehind@yahoo.com</a></font>
						</div>
					</row>
					<row>
						<div class="col-md-12">
							<font size="4px" color="white">&nbsp;<img src= "assets/images/skype.png" width="25px" height="25px">&nbsp;Skype <a href="#">code.behind</a></font>
						</div>
					</row>

				</div>
			
				<div class="col-md-4">
					<row>
						<div class="col-md-12">
							<font size="5px" color="white">Follow Us</font>
						</div>
					</row>
					<row>
						<div class="col-md-12">
							<font size="4px" color="white">&nbsp;<img src= "assets/images/FBtrans.png" width="20px" height="20px">&nbsp;<a href="#">/codebehind</a></font>
						</div>
					</row>
					<row>
						<div class="col-md-12">
							<font size="4px" color="white">&nbsp;<img src= "assets/images/ig.png" width="20px" height="20px">&nbsp;<a href="#">/codebehind</a></font>
						</div>
					</row>
					<row>
						<div class="col-md-12">
							<font size="4px" color="white">&nbsp;<img src= "assets/images/twitter.png" width="20px" height="20px">&nbsp;<a href="#">/codebehind</a></font>
						</div>
					</row>
				</div>	
			<div class="col-md-1">
			</div>
		</row>
	</div>
</body>
</html>